//Display/Show posts
fetch('https://jsonplaceholder.typicode.com/posts').then(res => res.json()).then(data => showPost(data));

const showPost = (posts) => {

	//hold the individual post
	let postEntries = ``

	posts.forEach((post) => {

			postEntries += `
			<div id= 'post-${post.id}'>
				<h3 id='post-title-${post.id}'>${post.title}</h3>
				<p id='post-body-${post.id}'>${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

};

//Create/add post

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data) //instead of showpost we console because we can't edit the fetched API
		alert('Successfully Added!')
		//null- this clears the input fielfs after the data is successfully added
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

//Edit Post

const editPost = ((postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
})

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method:'PUT',
		body: JSON.stringify({
				id:document.querySelector('#txt-edit-id').value,
				title:document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId: 1
			}),
		headers: {
			'Content-Type': 'application/json'
		}

	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert('Successfully Updated!')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})

})

//Delete Post


const deletePost = (postId) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    method: 'DELETE',
  });
      document.querySelector(`#post-${postId}`).remove();
};

/*
const deletePost = (postId) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    method: 'DELETE',
  });
    .then(res => res.json())
    .then(() => {
      alert(`Post ${postId} deleted`);
      document.querySelector(`#post-${postId}`).remove();
    });
};

*/


